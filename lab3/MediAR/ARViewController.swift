import UIKit
import RealityKit
import ARKit

class ARViewController: UIViewController {
    private let organs = ["heart", "liver", "brain", "kidney"]
    var selectedOrgan: String?
    
    var presenter: ARPresenter?
    
    let arView = ARView()
    private let coachingOverlay = ARCoachingOverlayView()
    let pickerView = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        renderUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        arView.session.delegate = presenter
        presenter?.setupARView()
        renderCoaching()
        arView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(arViewTapped(recognizer:))))
    }
    
    @objc
    func arViewTapped(recognizer: UITapGestureRecognizer) {
        presenter?.handleArViewTap(recognizer: recognizer)
    }
    
    private func renderUI() {
        view.addSubview(arView)
        renderARView()
        renderPickerView()
    }
    
    private func renderARView() {
        arView.translatesAutoresizingMaskIntoConstraints = false
        arView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        arView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        arView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        arView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    private func renderCoaching() {
        addCoaching()
        self.view.addSubview(coachingOverlay)
        coachingOverlay.translatesAutoresizingMaskIntoConstraints = false
        coachingOverlay.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        coachingOverlay.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        coachingOverlay.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        coachingOverlay.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
    }
    
    private func renderPickerView() {
        selectedOrgan = organs[0]
        
        pickerView.delegate = self
        pickerView.dataSource = self
        view.addSubview(pickerView)
        pickerView.translatesAutoresizingMaskIntoConstraints = false
        
        pickerView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        pickerView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        pickerView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        pickerView.heightAnchor.constraint(equalToConstant: 200).isActive = true
    }
}

extension ARViewController: ARCoachingOverlayViewDelegate {
    func addCoaching() {
        coachingOverlay.goal = .horizontalPlane
        coachingOverlay.session = arView.session
        coachingOverlay.delegate = self
        
        coachingOverlay.activatesAutomatically = true
    }
}

extension ARViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return organs.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return organs[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedOrgan = organs[row]
    }
}

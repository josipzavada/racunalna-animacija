import ARKit
import RealityKit

class ARPresenter: NSObject {
    unowned let view: ARViewController
    
    required init(viewController: ARViewController) {
        self.view = viewController
    }
    
    func handleArViewTap(recognizer: UITapGestureRecognizer) {
        let location = recognizer.location(in: view.arView)
        let results = view.arView.raycast(from: location, allowing: .estimatedPlane, alignment: .horizontal)
        
        if let firstResult = results.first {
            let anchor = ARAnchor(name: "Anchor", transform: firstResult.worldTransform)
            view.arView.session.add(anchor: anchor)
        } else {
            print("Object placement failed!")
        }
    }
    
    func placeObject(named entityName: String, for anchor: ARAnchor) {
        guard let selectedOrgan = view.selectedOrgan else { return }
        let entity = try! ModelEntity.loadModel(named: selectedOrgan)
        
        entity.generateCollisionShapes(recursive: true)
        view.arView.installGestures([.all], for: entity)
        let anchorEntity = AnchorEntity(anchor: anchor)
        anchorEntity.addChild(entity)
        view.arView.scene.addAnchor(anchorEntity)
    }
    
    func setupARView() {
//        arView.automaticallyConfigureSession = false
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = [.horizontal, .vertical]
        configuration.environmentTexturing = .automatic
        view.arView.session.run(configuration)
    }
}

extension ARPresenter: ARSessionDelegate {
    func session(_ session: ARSession, didAdd anchors: [ARAnchor]) {
        for anchor in anchors {
            if let anchorName = anchor.name, anchorName == "Anchor" {
                placeObject(named: anchorName, for: anchor)
            }
        }
    }
}

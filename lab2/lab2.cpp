#include <GL/glut.h>
#include <stdio.h>
#include <math.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>

#include <string>
#include <fstream>
#include <vector>
#include <iostream>

using namespace std;

class Source {
public:
	float x, y, z;
	int q;
	float cR, cG, cB;
	double size;
};

class Vertex {
public:
	float x, y, z;
	Vertex(float x1, float y1, float z1) {
		x = x1; y = y1; z = z1;
	}
};

class Particle {
public:
	float x, y, z;
	float r, g, b;
	float v;
	int t;
	float sX, sY, sZ;
	float xAxis, yAxis, zAxis;
	double kut; double size;
};

vector<Particle> particles;
Source source;

GLuint window; 
GLuint sub_width = 512, sub_height = 512;

void myDisplay			();
void myIdle				();
void myReshape			(int width, int height);
void myKeyboard			(unsigned char theKey, int mouseX, int mouseY);

void drawSource		();
void drawParticles		();
GLuint LoadTextureRAW( const char * filename, int wrap );

double pi = 3.14159;
int maxT = 200;
float step = 0.1;
int tempQ = -1;
  GLuint tex;
  Vertex camera(0.0, 0.0, 75.0);

int main(int argc, char ** argv)
{
  source.x = 0.0; source.y = -10; source.z = 50.0;
  source.q = 10;
  source.cB = 1.0; source.cG = 1.0; source.cR = 1.0;
  source.size = 0.4;
  srand (time(NULL));
  
  	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(sub_width,sub_height);
	glutInitWindowPosition(100,100);
	glutInit(&argc, argv);

	window = glutCreateWindow("Lab 2");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay);
	glutIdleFunc(myIdle);
  	tex = LoadTextureRAW("cestica.bmp", 0);

  	glBlendFunc(GL_SRC_ALPHA,GL_ONE);
	glEnable(GL_BLEND);

	
    glEnable( GL_TEXTURE_2D );
    glBindTexture( GL_TEXTURE_2D, tex );

	glutMainLoop();
    	return 0;
}

void myReshape(int width, int height)
{
	sub_width = width;
    sub_height = height;

    glViewport(0, 0, sub_width, sub_height);
	
	glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
 
    gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,150.0f);
 
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

	glLoadIdentity();
	glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
	glClear( GL_COLOR_BUFFER_BIT );
	glPointSize(1.0);
	glColor3f(0.0f, 0.0f, 0.0f);
}

int t = 0;

void myDisplay()
{

 	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	
	glTranslatef(camera.x, camera.y, -camera.z);

	drawParticles();

	glutSwapBuffers();
}

int currentTime = 0; int previousTime = 0;

void myIdle() {
	currentTime = glutGet(GLUT_ELAPSED_TIME);
	int timeInterval = currentTime - previousTime;
	if(timeInterval > 10) {
		
		if(source.q > 0) {
			int n = rand() % source.q + 1;
			for (int j = 0; j < n; j++) {
				double x, y, z, norm;
				x = (rand()%2000-1000);
				y = (rand()%1000);
				z = (rand()%2000-1000);
				norm = pow(pow(x, 2.0) + pow(y, 2.0) + pow(z, 2.0), 0.5);
				x /= norm; y /= norm; z /= norm;
				Particle c;
				c.x = source.x; c.z = source.z; c.y = source.y; 
				c.r = source.cR; c.g = source.cG; c.b = source.cB; c.v = 0.8;
				c.sX = x; c.sY = y; c.sZ = z;
				c.t = maxT;
				c.size = source.size;
				particles.push_back(c);
			}
		}

		for (int j =  particles.size() - 1; j >= 0; j--) {
			particles.at(j).y += particles.at(j).v * particles.at(j).sY * step;
			particles.at(j).z += particles.at(j).v * particles.at(j).sZ * (step / 5);
			particles.at(j).x += particles.at(j).v * particles.at(j).sX * (step / 5);

			particles.at(j).t--;

			particles.at(j).b = pow((float)particles.at(j).t / (float)maxT, 0.5);
			particles.at(j).g = pow((float)particles.at(j).t / (float)maxT, 2);
			particles.at(j).r = (float)particles.at(j).t / (float)maxT;
			if (particles.at(j).t <= 0) {
				particles.erase(particles.begin()+j);
			}
		}

		myDisplay();
		previousTime = currentTime;
	}
}

void drawParticles() {
	for (int j = 0; j < particles.size(); j++) {
		Particle c = particles.at(j);
		glColor3f(c.r, c.g, c.b);
		glTranslatef(c.x, c.y, c.z);
		glRotatef(c.kut, c.xAxis, c.yAxis, c.zAxis);
		glBegin(GL_QUADS);
		
		glTexCoord2d(0.0,0.0); glVertex3f(-c.size, -c.size, 0.0);
		glTexCoord2d(1.0,0.0); glVertex3f(-c.size, +c.size, 0.0);
		glTexCoord2d(1.0,1.0); glVertex3f(+c.size, +c.size, 0.0);
		glTexCoord2d(0.0,1.0); glVertex3f(+c.size, -c.size, 0.0);

		glEnd();
		glRotatef(-c.kut, c.xAxis, c.yAxis, c.zAxis);
		glTranslatef(-c.x, -c.y, -c.z);
	}
}

GLuint LoadTextureRAW( const char * filename, int wrap )
{
    GLuint texture;
    int width, height;
    BYTE * data;
    FILE * file;
    // open texture data
    file = fopen( filename, "rb" );
    if ( file == NULL )  {
		return 0;
	}

    // allocate buffer
    width = 256;
    height = 256;
    data =(BYTE*) malloc( width * height * 3 );

    // read texture data
    fread( data, width * height * 3, 1, file );
    fclose( file );

    // allocate a texture name
    glGenTextures( 1, &texture );

    // select our current texture
    glBindTexture( GL_TEXTURE_2D, texture );

    // select modulate to mix texture with color for shading
    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );

    // when texture area is small, bilinear filter the closest mipmap
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                     GL_LINEAR_MIPMAP_NEAREST );
    // when texture area is large, bilinear filter the first mipmap
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    // if wrap is true, the texture wraps over at the edges (repeat)
    //       ... false, the texture ends at the edges (clamp)
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
                     wrap ? GL_REPEAT : GL_CLAMP );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,
                     wrap ? GL_REPEAT : GL_CLAMP );

    // build our texture mipmaps
    gluBuild2DMipmaps( GL_TEXTURE_2D, 3, width, height,
                       GL_RGB, GL_UNSIGNED_BYTE, data );

    // free buffer
    free( data );

    return texture;
}
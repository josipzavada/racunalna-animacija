#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <math.h>
#include <GL/glut.h>

using namespace std;

class Model
{
private:
    class Face
    {
    public:
        int edge;
        int *vertices;

        Face(int edge, int *vertices)
        {
            this->edge = edge;
            this->vertices = vertices;
        }
    };

    vector<float *> vertices;
    vector<Face> faces;

public:
    void load(const char *filename)
    {
        string line;
        vector<string> lines;

        ifstream in(filename);

        if (!in.is_open())
        {
            cout << "Cannot load model";
            return;
        }

        while (!in.eof())
        {
            getline(in, line);
            lines.push_back(line);
        }

        in.close();

        float a, b, c;
        for (string &line : lines)
        {
            if (line[0] == 'v')
            {
                if (line[1] == ' ')
                {
                    sscanf(line.c_str(), "v %f %f %f", &a, &b, &c);
                    vertices.push_back(new float[3]{a, b, c});
                }
            }
            else if (line[0] == 'f')
            {
                int v0, v1, v2;
                sscanf(line.c_str(), "f %d %d %d", &v0, &v1, &v2);
                int *v = new int[3]{v0 - 1, v1 - 1, v2 - 1};
                faces.push_back(Face(3, v));
            }
        }
    }

    void print()
    {
        for (Face i : this->faces)
        {
            cout << "Vertecies:\t\t" << i.vertices[0] << i.vertices[1] << i.vertices[2] << "\n";
        }
    }
};

class Vertex
{
public:
    float x, y, z;
    Vertex(float x1, float y1, float z1)
    {
        x = x1;
        y = y1;
        z = z1;
    }
};

Vertex *vertices, *spline_verticies, *tangents, *allTangents;
int tangents_count, all_tangents_count, vertices_count, segments_count;
Model model;

Vertex rotation_axis(0.0, 0.0, 0.0);
Vertex s(0.0, 1.0, 0.0);
Vertex e(0.0, 0.0, 0.0);

Vertex *load_vertices(string file_name)
{

    vector<string> lines;
    ifstream vertices_file(file_name);
    string line;

    while (getline(vertices_file, line))
    {
        lines.push_back(line);
    }

    vertices_count = lines.size();
    segments_count = vertices_count - 3;

    Vertex *vertices = (Vertex *)malloc(vertices_count * sizeof(Vertex));

    for (int i = 0; i < vertices_count; ++i)
    {
        float d1, d2, d3;
        char *text = new char[lines.at(i).size() + 1];
        std::copy(lines.at(i).begin(), lines.at(i).end(), text);
        sscanf(text, "%f %f %f", &d1, &d2, &d3);
        Vertex v(d1, d2, d3);
        vertices[i] = v;
    }

    return vertices;
}

void create_spline_and_tangents()
{
    model.load("747.obj");

    vertices = load_vertices("dots.txt");
    segments_count = vertices_count - 3;

    spline_verticies = (Vertex *)malloc(100 * segments_count * sizeof(Vertex));
    tangents = (Vertex *)malloc(segments_count * 8 * sizeof(Vertex));
    allTangents = (Vertex *)malloc(segments_count * 2 * 100 * sizeof(Vertex));

    for (int i = 0; i < segments_count; ++i)
    {
        Vertex v0 = vertices[i];
        Vertex v1 = vertices[i + 1];
        Vertex v2 = vertices[i + 2];
        Vertex v3 = vertices[i + 3];

        for (int t = 0; t < 100; t++)
        {
            double j = t / 100.0;
            float f1 = (-pow(j, 3.0) + 3 * pow(j, 2.0) - 3 * j + 1) / 6.0;
            float f2 = (3 * pow(j, 3.0) - 6 * pow(j, 2.0) + 4) / 6.0;
            float f3 = (-3 * pow(j, 3.0) + 3 * pow(j, 2.0) + 3 * j + 1) / 6.0;
            float f4 = pow(j, 3.0) / 6.0;

            float x0 = f1 * v0.x + f2 * v1.x + f3 * v2.x + f4 * v3.x;
            float y0 = f1 * v0.y + f2 * v1.y + f3 * v2.y + f4 * v3.y;
            float z0 = f1 * v0.z + f2 * v1.z + f3 * v2.z + f4 * v3.z;

            spline_verticies[100 * i + t].x = x0;
            spline_verticies[100 * i + t].y = y0;
            spline_verticies[100 * i + t].z = z0;

            float t1 = 0.5 * (-pow(j, 2.0) + 2 * j - 1);
            float t2 = 0.5 * (3 * pow(j, 2.0) - 4 * j);
            float t3 = 0.5 * (-3 * pow(j, 2.0) + 2 * j + 1);
            float t4 = 0.5 * (pow(j, 2.0));

            float x1 = t1 * v0.x + t2 * v1.x + t3 * v2.x + t4 * v3.x;
            float y1 = t1 * v0.y + t2 * v1.y + t3 * v2.y + t4 * v3.y;
            float z1 = t1 * v0.z + t2 * v1.z + t3 * v2.z + t4 * v3.z;

            if (t % 50 == 0)
            {
                tangents[tangents_count].x = x0;
                tangents[tangents_count].y = y0;
                tangents[tangents_count].z = z0;
                tangents_count++;

                tangents[tangents_count].x = tangents[tangents_count - 1].x + x1 / 3;
                tangents[tangents_count].y = tangents[tangents_count - 1].y + y1 / 3;
                tangents[tangents_count].z = tangents[tangents_count - 1].z + z1 / 3;
                tangents_count++;
            }

            allTangents[all_tangents_count].x = x0;
            allTangents[all_tangents_count].y = y0;
            allTangents[all_tangents_count].z = z0;
            all_tangents_count++;

            allTangents[all_tangents_count].x = allTangents[all_tangents_count - 1].x + x1 / 3;
            allTangents[all_tangents_count].y = allTangents[all_tangents_count - 1].y + y1 / 3;
            allTangents[all_tangents_count].z = allTangents[all_tangents_count - 1].z + z1 / 3;
            all_tangents_count++;
        }
    }
}

void changeSize(int w, int h)
{
    if (h == 0)
        h = 1;

    float ratio = w * 1.0 / h;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, w, h);
    gluPerspective(45.0f, ratio, 0.1f, 100.0f);
    glMatrixMode(GL_MODELVIEW);
}

int iteration = 0;
float pi = 3.14159265;

void renderScene(void)
{

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity();

    gluLookAt(0.0f, 0.0f, 20.0f,
              0.0f, 0.0f, 0.0f,
              0.0f, 1.0f, 0.0f);


    glBegin(GL_LINE_STRIP);
    for (int i = 0; i < (100 * segments_count); ++i)
    {
        glVertex3f(spline_verticies[i].x - 50.0f, spline_verticies[i].y, spline_verticies[i].z);
    }
    glEnd();

    // glBegin(GL_LINES);
    // for (int i = 0; i < segments_count * 2; ++i){
    //     glVertex3f(allTangents[i].x - 50.0f, allTangents[i].y, allTangents[i].z);
    //     glVertex3f(allTangents[i+1].x - 50.0f, allTangents[i+1].y, allTangents[i+1].z);
    // }
    // glEnd();

    glTranslatef(spline_verticies[iteration].x - 50.0f, spline_verticies[iteration].y, spline_verticies[iteration].z);

    e.x = allTangents[2 * iteration + 1].x - allTangents[2 * iteration].x;
    e.y = allTangents[2 * iteration + 1].y - allTangents[2 * iteration].y;
    e.z = allTangents[2 * iteration + 1].z - allTangents[2 * iteration].z;

    rotation_axis.x = s.y * e.z - e.y * s.z;
    rotation_axis.y = e.x * s.z - s.x * e.z;
    rotation_axis.z = s.x * e.y - s.y * e.x;

    double apsS = pow(pow((double)s.x, 2.0) + pow((double)s.y, 2.0) + pow((double)s.z, 2.0), 0.5);
    double apsE = pow(pow((double)e.x, 2.0) + pow((double)e.y, 2.0) + pow((double)e.z, 2.0), 0.5);
    double se = s.x * e.x + s.y * e.y + s.z * e.z;
    double kut = acos(se / (apsS * apsE));
    kut = kut / (2 * 3.14159265f) * 360;
    glRotatef(-kut, 0.0f, 0.0f, 1.0f);

    glBegin(GL_TRIANGLES);
    glVertex3f(-2.0f, -2.0f, 0.0f);
    glVertex3f(2.0f, 0.0f, 0.0);
    glVertex3f(0.0f, 2.0f, 0.0);
    glEnd();

    if (iteration < (segments_count * 100) - 1)
    {
        iteration++;
    }

    glutSwapBuffers();
}

int main(int argc, char **argv)
{
    create_spline_and_tangents();

    // init GLUT and create window
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowPosition(100, 100);
    glutInitWindowSize(1300, 320);
    glutCreateWindow("Prva laboratorijska vježba");

    // register callbacks
    glutDisplayFunc(renderScene);
    glutReshapeFunc(changeSize);
    glutIdleFunc(renderScene);

    // enter GLUT event processing cycle
    glutMainLoop();

    return 1;
}
